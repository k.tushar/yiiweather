//fetching city details
$("#fetch").click(function(){
    let city_name=$("#city_name").val();
    $.ajax({
    type:"POST",
    url: "?r=site/get-data",
    data:"city="+city_name,
    success: function(data){
        console.log(data);

        if(data == 400 || data == 404){
            document.getElementById( 'dBlock' ).style.display = 'none';
            document.getElementById( 'tush' ).style.display = 'block';
        }else {
            document.getElementById( 'dBlock' ).style.display = 'block';
            document.getElementById( 'tush' ).style.display = 'none';
            
            var result=JSON.parse(data);
        
            var a = new Date(result.dt * 1000);
            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = date + ' ' + month ;
        
        var tempr = result.temp - 273.15;
        var rTemp = parseInt(tempr);
        $("#temperature").html(rTemp+'\xB0');
        $("#description").html(result.description);
        document.getElementById("icon").setAttribute('src', 'http://openweathermap.org/img/wn/'+result.icon+'@4x.png');
        $("#name").html(result.name);
        $("#dt").html(time);
            
        }
        
    }
});
});