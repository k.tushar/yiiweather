<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>

<style>
    @import url(https://fonts.googleapis.com/css?family=Poiret+One);
     body {
         font-family: Poiret One;
         }
    .rowWidth{
        background: #fff;
        
    }
    
    .rowWidthd{
        padding: 18px;
        background: #18191a;
    }
    
    #dataTable {
        
        text-align:center;
        width: 93%;
        margin:20px ;
    }
    
    #rectangle {
        width: 100%;
        background: #a63e35;
      }
      table {
        border-collapse: collapse;
        border-radius: 1em;
        overflow: hidden;
      }
      
      .rowWidthdate{
          background: #34d6d9;
          padding: 18px;
          color:#fff;
          font-size: 30px;
          font-weight:500px; 
      }
     
      #temperature{
          font-size: 50px;
          color: #fff;
      }
      
      #description{
          font-size: 24px;
          text-transform: uppercase;
          color: #fff;
          text-align: left;
      }
      
      #name{
          font-size: 15px;
          color: #fff;
          text-align: left;
      }
      
      .jumbo{
          margin-bottom: 40px;
      }
      </style>

      
    <div class="jumbo text-center bg-transparent">
        <h3><b>Check Weather According to the city</b></h3>
    </div>

    <div class="body-content">

        <div class="row">
            
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="col-sm-12">
                            <?= Html::input('text','city_name', '', $options=['class'=>'form-control','id'=>'city_name','maxlength'=>10 , 'placeholder'=>'City Name..']) ?>
                </div>
                <br>
                <div class="col-sm-12">
                            <?= Html::submitButton('Submit', ['name' => 'submit', 'class' => 'btn btn-primary', 'id'=>'fetch']) ?>
                </div><br>
                
                <div id="tush" style="display:block;"><b>No Record Found</b> </div>
                    <div id="dBlock" style="display:none">
                        <div id="rectangle" class="border"> 
                            <div class="wrapper">
                                <table id="dataTable" style="padding:10px;"> 
                                    <tr> 
                                        <th class="rowWidth" colspan="3"><img id="icon" src="" width="150"/></th> 
                                     </tr>
                                     <tr> 
                                         <td class="rowWidthd" id="temperature"></td>
                                         <div class="desc">
                                             <td class="rowWidthd"><p id="description"></p><p id="name"></p></td>
                                         </div>
                                         <td class="rowWidthdate" id="dt"></td> 
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3"></div>
        </div>
        
    </div>
