<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>

    <div class="jumbotron text-center bg-transparent">
<!--        <h1 class="display-4">Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>-->
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
               
              <?php
$form = ActiveForm::begin([
                                'action' => ['get-data'],
                                 'method' => 'post',
                                'layout' => 'horizontal',
                                'class' => 'form-horizontal',
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-8',
                                        //                        'offset' => 'col-sm-offset-4',
                                        'error' => '',
                                        'hint' => '',
                                    ],
                                ],
                    ]);
                    ?> 
                
                <div class="col-sm-12">
                            <?= Html::input('text','city_name', '', $options=['class'=>'form-control','id'=>'city_name','maxlength'=>10]) ?>
                        </div><br>
                <div class="col-sm-12">
                            <?= Html::submitButton('Submit', ['name' => 'submit', 'class' => 'btn btn-primary', 'id'=>'fetch']) ?>
                </div>
                        
                <h2>Your result</h2>

                <p id="ajax_result">

                </p>

                <!--<p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>-->
                       <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
